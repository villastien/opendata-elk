#Récupération des données OpenData
Les données sont mises en place par Insee sur la page https://www.data.gouv.fr/fr/datasets/ficher-des-prenoms-de-1900-a-2018/ .
Le fichier est mis à jour une fois par an et contient toutes les données.

#Création de l'index
Dans le dev tools de Kibana : 
~~~
PUT prenoms 
{
  "mapping": {
    "_doc": {
      "properties": {
        "annee": {
          "type": "keyword"
        },
        "departement": {
          "type": "keyword"
        },
        "nombre": {
          "type": "long"
        },
        "prenom": {
          "type": "keyword"
        },
        "sexe": {
          "type": "keyword"
        }
      }
    }
  }
}
~~~
#Execution du script logstash
~~~
logstash -f ./import.conf
~~~

#Amélioratations à faire
-Mettre en place un filtre pour ne pas reindexer les données déjà intégrées les années précédentes.
