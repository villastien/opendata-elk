#Récupération des données OpenData
Les données sont mises en place par le ministère de l'économie et des finances sur la page https://www.data.gouv.fr/fr/datasets/demandes-de-valeurs-foncieres/ .
Le fichier est mis à jour une fois par trimestre et contient toutes les données de l'année.

#Création de l'index
Dans le dev tools de Kibana : 
~~~
PUT valeur_fonciere 
{
  "mapping": {
    "_doc": {
      "properties": {
        "B_T_Q": {
          "type": "keyword"
        },
        "adresse": {
          "properties": {
            "commune": {
              "properties": {
                "id": {
                  "type": "keyword"
                },
                "label": {
                  "type": "keyword"
                }
              }
            },
            "cp": {
              "type": "keyword"
            },
            "departement": {
              "type": "keyword"
            },
            "voie": {
              "properties": {
                "id": {
                  "type": "keyword"
                },
                "label": {
                  "type": "keyword"
                },
                "num": {
                  "type": "keyword"
                },
                "type": {
                  "type": "keyword"
                }
              }
            }
          }
        },
        "articles": {
          "properties": {
            "gci1": {
              "type": "keyword"
            },
            "gci2": {
              "type": "keyword"
            },
            "gci3": {
              "type": "keyword"
            },
            "gci4": {
              "type": "keyword"
            },
            "gci5": {
              "type": "keyword"
            }
          }
        },
        "codeserviceCH": {
          "type": "keyword"
        },
        "local": {
          "properties": {
            "id": {
              "type": "keyword"
            },
            "type": {
              "properties": {
                "id": {
                  "type": "keyword"
                },
                "label": {
                  "type": "keyword"
                }
              }
            }
          }
        },
        "lot": {
          "properties": {
            "n1": {
              "properties": {
                "label": {
                  "type": "keyword"
                },
                "surfaceCarrez": {
                  "type": "keyword"
                }
              }
            },
            "n2": {
              "properties": {
                "label": {
                  "type": "keyword"
                },
                "surfaceCarrez": {
                  "type": "keyword"
                }
              }
            },
            "n3": {
              "properties": {
                "label": {
                  "type": "keyword"
                },
                "surfaceCarrez": {
                  "type": "keyword"
                }
              }
            },
            "n4": {
              "properties": {
                "label": {
                  "type": "keyword"
                },
                "surfaceCarrez": {
                  "type": "keyword"
                }
              }
            },
            "n5": {
              "properties": {
                "label": {
                  "type": "keyword"
                },
                "surfaceCarrez": {
                  "type": "keyword"
                }
              }
            },
            "nb": {
              "type": "integer"
            }
          }
        },
        "mutation": {
          "properties": {
            "date": {
              "type": "date"
            },
            "nature": {
              "type": "keyword"
            }
          }
        },
        "natureCulture": {
          "type": "keyword"
        },
        "natureCultureSpeciale": {
          "type": "keyword"
        },
        "noDisposition": {
          "type": "keyword"
        },
        "noVolume": {
          "type": "keyword"
        },
        "nombrePiecesPrincipales": {
          "type": "integer"
        },
        "noplan": {
          "type": "keyword"
        },
        "prefixeSection": {
          "type": "keyword"
        },
        "referenceDocument": {
          "type": "keyword"
        },
        "section": {
          "type": "keyword"
        },
        "surfaceReelleBati": {
          "type": "integer"
        },
        "surfaceTerrain": {
          "type": "integer"
        },
        "valeurFonciere": {
          "type": "float"
        }
      }
    }
  }
}
~~~
#Execution du script logstash
~~~
logstash -f ./import.conf
~~~

#Amélioratations à faire
-Trouver une solution pour éviter de devoir supprimer les données de l'année après une mise à jour semestrielle.
